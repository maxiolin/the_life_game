class Utils {
  private static _grid = [];

  static setCell(x, y, alive) {
    this._grid[x][y] = { alive: alive };
    return this._grid;
  }

  static createGrid(x, y) {
    this._grid = Array.from({ length: x }, () =>
      Array.from({ length: y }, () => ({ alive: false }))
    );
    return this._grid;
  }

  private static _checkLife(x, y, grid) {
    let counter = 0;

    try {
      grid[x - 1][y - 1].alive ? counter++ : null;
    } catch {
      null;
    }

    try {
      grid[x][y - 1].alive ? counter++ : null;
    } catch {
      null;
    }

    try {
      grid[x + 1][y - 1].alive ? counter++ : null;
    } catch {
      null;
    }

    try {
      grid[x - 1][y].alive ? counter++ : null;
    } catch {
      null;
    }

    try {
      grid[x + 1][y].alive ? counter++ : null;
    } catch {
      null;
    }

    try {
      grid[x - 1][y + 1].alive ? counter++ : null;
    } catch {
      null;
    }

    try {
      grid[x][y + 1].alive ? counter++ : null;
    } catch {
      null;
    }

    try {
      grid[x + 1][y + 1].alive ? counter++ : null;
    } catch {
      null;
    }

    if (!grid[x][y].alive) {
      return counter === 3 ? true : false;
    } else {
      if (counter >= 2 && counter <= 3) {
        return true;
      }

      if (counter < 2 || counter > 3) {
        return false;
      }
    }
  }

  static computeNewState() {
    const prevStateGrid = JSON.parse(JSON.stringify(this._grid));
    for (let i = 0; i < this._grid.length; i++) {
      for (let j = 0; j < this._grid[i].length; j++) {
        this._grid[i][j] = { alive: this._checkLife(i, j, prevStateGrid) };
      }
    }
    return this._grid;
  }
}

export default Utils;
