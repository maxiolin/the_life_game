import React, { Component } from "react";
import IGridProps from "./interfaces/IGridProps";
import Column from "./Column";

class Grid extends Component<IGridProps> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="grid">
        {this.props.grid.map((column, columnNum) => (
          <Column
            key={columnNum}
            column={column}
            index={columnNum}
            giveDeath={this.props.giveDeath}
            giveLife={this.props.giveLife}
          />
        ))}
      </div>
    );
  }
}

export default Grid;
