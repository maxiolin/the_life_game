interface ICellProps {
  column: number;
  row: number;
  giveLife: (column: any, row: any) => void;
  giveDeath: (column: any, row: any) => void;
  alive: boolean;
}

export default ICellProps;
