interface IGridProps {
  grid: Array<any>;
  giveLife: (column: any, row: any) => void;
  giveDeath: (column: any, row: any) => void;
}

export default IGridProps;
