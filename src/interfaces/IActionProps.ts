interface IActionsProps {
    startGame: () => void,
    createGrid: (width: Number, height: Number) => void
}

export default IActionsProps;