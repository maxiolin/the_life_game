import React from "react";
import ReactDOM from "react-dom";
import Utils from "./utils/Utils";
import Actions from "./Actions";
import Grid from "./Grid";

class Game extends React.Component<{}, { grid: Array<any>; start: boolean }> {
  constructor(props) {
    super(props);

    this.state = {
      grid: [],
      start: false,
    };

    this.createGrid = this.createGrid.bind(this);
    this.giveLife = this.giveLife.bind(this);
    this.giveDeath = this.giveDeath.bind(this);
    this.startGame = this.startGame.bind(this);
  }

  giveLife(column, row) {
    const grid = Utils.setCell(column, row, true);
    this.setState(() => ({ grid }));
  }

  giveDeath(column, row) {
    this.setState(() => ({
      grid: Utils.setCell(column, row, false),
    }));
  }

  startGame() {
    setInterval(() => {
      this.setState(() => ({
        grid: Utils.computeNewState(),
      }));
    }, 500);
  }

  createGrid(columns, rows) {
    const grid = Utils.createGrid(columns, rows);
    this.setState(() => ({ grid }));
  }

  render() {
    return (
      <div>
        <h1>The Game Life</h1>
        <Actions startGame={this.startGame} createGrid={this.createGrid} />
        <Grid
          grid={this.state.grid}
          giveDeath={this.giveDeath}
          giveLife={this.giveLife}
        />
      </div>
    );
  }
}

ReactDOM.render(<Game />, document.getElementById("app"));
