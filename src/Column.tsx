import React from "react";
import Cell from "./Cell";

const Column: React.FC<{
  column: Array<any>;
  index: number;
  giveLife: (column: any, row: any) => void;
  giveDeath: (column: any, row: any) => void;
}> = ({ column, index, giveDeath, giveLife }) => {
  return (
    <div className="column">
      {column.map((cell, row) => (
        <Cell
          key={`(${index},${row})`}
          column={index}
          row={row}
          alive={cell.alive}
          giveDeath={giveDeath}
          giveLife={giveLife}
        />
      ))}
    </div>
  );
};

export default Column;
