import React from "react";
import IActionsProps from "./interfaces/IActionProps";

const Actions: React.FC<IActionsProps> = ({ startGame, createGrid }) => {
  function handleSubmit(e) {
    e.preventDefault();
    const width = parseInt(e.target.elements.width.value.trim());
    const height = parseInt(e.target.elements.height.value.trim());
    createGrid(width, height);
  }

  return (
    <div className="actions">
      <form className="inline" onSubmit={handleSubmit}>
        <input name="width" placeholder="Width" type="number"></input>
        <input name="height" placeholder="Height" type="number"></input>
        <button>Create Grid</button>
      </form>
      <button className="inline" onClick={startGame}>
        Start
      </button>
    </div>
  );
};

export default Actions;
