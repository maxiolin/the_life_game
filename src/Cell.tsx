import React from "react";
import ICellProps from "./interfaces/ICellProps";
import "./Index.css";

class Cell extends React.Component<ICellProps> {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.alive !== this.props.alive) console.log("alive");
  }

  handleClick() {
    if (this.props.alive === true) {
      this.props.giveDeath(this.props.column, this.props.row);
    } else {
      this.props.giveLife(this.props.column, this.props.row);
    }
  }

  render() {
    return (
      <div
        className={this.props.alive ? "cell-black" : "cell-white"}
        onClick={this.handleClick}
      ></div>
    );
  }
}

export default Cell;
